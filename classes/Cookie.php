<?php

class Cookie
{

    public static function put($name, $value, $expiry)
    {
        if (setcookie($name, $value, time() + $expiry, "/")) {
            return true;
        }
        return false;
    }

    public static function delete($name)
    {
        self::put($name, "", time() - 1);
    }
}
