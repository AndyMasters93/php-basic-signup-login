<?php

class Validate
{
    private $_passed = false,
    $_errors = array(),
    $_db = null;

    public function __construct()
    {
        $this->_db = DB::getInstance();
    }

    public function check($source, $items = array())
    {
        foreach ($items as $item => $rules) {
            foreach ($rules as $rule => $ruleValue) {

                $value = trim($source[$item]);
                $item = escape($item);
                $itemFormatted = ucfirst(strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $item)));
                $ruleValueFormatted = ucfirst(strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $ruleValue)));

                if ($rule === "required" && empty($value)) {

                    $this->addError("$itemFormatted is required");

                } elseif (!empty($value)) {

                    switch ($rule) {

                        case "min":
                            if (strlen($value) < $ruleValue) {
                                $this->addError("$itemFormatted must be a minimum of $ruleValue characters.");
                            }
                            break;

                        case "max":
                            if (strlen($value) > $ruleValue) {
                                $this->addError("$itemFormatted must be a maximum of $ruleValue characters.");
                            }
                            break;

                        case "matches":
                            if ($value != $source[$ruleValue]) {
                                $this->addError("$ruleValueFormatted must match $itemFormatted");
                            }
                            break;

                        case "unique":
                            $check = $this->_db->get($ruleValue, array($item, "=", $value));
                            if ($check->count()) {
                                $this->addError("$itemFormatted already exists");
                            }
                            break;
                    }
                }
            }
        }
        if (empty($this->_errors)) {
            $this->_passed = true;
        }
        return $this;
    }

    public function errors()
    {
        return $this->_errors;
    }

    public function passed()
    {
        return $this->_passed;
    }

    private function addError($error)
    {
        $this->_errors[] = $error;
    }
}
