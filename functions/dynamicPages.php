<?php

function getPage($dir, $filename, $default = false)
{

    $root = $_SERVER['DOCUMENT_ROOT'] . '/php-basic-signup-login/';
    $path = $root . $dir;

    if (is_dir($path)) {

        if (file_exists($path . '/' . $filename . '.php')) {

            include $path . '/' . $filename . '.php';
            return true;

        } elseif (file_exists($path . '/' . $filename . '.html')) {

            include $path . '/' . $filename . '.html';
            return true;

        } elseif ($default) {

            if (file_exists($path . '/' . $default . '.phpa')) {

                include $path . '/' . $default . '.php';
                return true;

            } elseif (file_exists($path . '/' . $default . '.html')) {

                include $path . '/' . $default . '.html';
                return true;

            } else {

                Redirect::to(404);
            }
        }
    }
    return false;
}
