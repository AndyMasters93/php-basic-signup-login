<?php
    require_once 'core/init.php';    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="includes/css/main.css">
	    <link rel="stylesheet" href="includes/css/bootstrap.min.css">

    </head>
    <body>

        <header>

            <nav class="navbar navbar-expand-md navbar-light bg-light">                
                <a class="navbar-brand" href="/php-basic-signup-login">PHP Refresh</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item dropdown">

                            <?php

                                $user = new User();

                                if($user->isLoggedIn()) {
                                    ?>
                                        <a class="nav-link dropdown-toggle" href="#" id="logoutDropdown" data-toggle="dropdown" role="button">
                                            <?php echo escape($user->data()->username); ?>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="/php-basic-signup-login/logout">Logout</a>                                   
                                        </div>
                                    <?php
                                } else {
                                    ?>
                                        <a class="nav-link dropdown-toggle" href="#" id="loginDropdown" data-toggle="dropdown" role="button">
                                            Sign Up / Log In
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="/php-basic-signup-login/login">Log In</a>
                                            <a class="dropdown-item" href="/php-basic-signup-login/signup">Sign up</a>
                                        </div>
                                    <?php
                                }
                            ?>
                        </li>
                    </ul>                    
                </div>
            </nav>
        </header>

        <div class="container">
            <?php
                if(isset($_GET['page'])) {
                    getPage('pages', $_GET['page'], 'home');
                } else {
                    getPage('pages', 'home');
                }

                include 'footer.php';
            ?>
        </div> <!-- container -->
  	</body>
</html>
