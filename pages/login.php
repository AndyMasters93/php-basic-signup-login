<?php

if (Session::exists('signupConfirm')) {
    echo
    "<div class='alert alert-success' role='alert'>"
    . Session::flash('signupConfirm') .
        "</div>";
}

if (Input::exists()) {

    if (Token::check(Input::get("token"))) {

        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            "username" => array("required" => true),
            "password" => array("required" => true),
        ));

        if ($validation->passed()) {

            $user = new User();
            $remember = (input::get("remember") === "on") ? true : false;

            $login = $user->login(Input::get("username"), Input::get("password"), $remember);

            if ($login) {
                Redirect::to("/php-basic-signup-login");
            } else {
                echo "
                    <div class='alert alert-danger' role='alert'>
                        Log In was unsuccessful.
                    </div>
                ";
            }

        } else {
            foreach ($validation->errors() as $error) {
                echo "
                    <div class='alert alert-danger' role='alert'>
                        {$error}
                    </div>
                ";
            }
        }
    }
}

?>

<form method="post">
    
    <div class="form-group row">
        <label for="username" class="col-sm-2 col-form-label">Username</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="username" id="username">
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="remember"  class="col-sm-2 col-form-label">Remember Me</label>
        <div class="col-sm-10">
            <input type="checkbox" name="remember" id="remember">
        </div>
    </div>

    <input type="hidden" name="token" value="<?=Token::generate();?>">

    <div class="form-group row">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Log In</button>
        </div>
    </div>
</form>