<?php

if (Input::exists()) {

    if (Token::check(Input::get("token"))) {

        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            "username" => array(
                "required" => true,
                "min" => 6,
                "max" => 32,
                "unique" => "user",
            ),
            "password" => array(
                "required" => true,
                "min" => 6,
            ),
            "passwordConfirmation" => array(
                "required" => true,
                "matches" => "password",
            ),
            "name" => array(
                "required" => true,
                "min" => 2,
                "max" => 32,
            ),
        ));

        if ($validation->passed()) {

            $user = new User();
            $salt = Hash::salt(32);

            try {
                $user->create(array(
                    "username" => Input::get("username"),
                    "password" => Hash::make(Input::get("password"), $salt),
                    "salt" => $salt,
                    "name" => Input::get("name"),
                    "joined" => date("Y-m-d H:i:s"),
                    "permissionsGroup" => 1,
                ));

                Session::flash("signupConfirm", "You have been registered and can now login");
                Redirect::to("login");
            } catch (Exception $e) {
                die($e->getMessage());
            }

        } else {
            foreach ($validation->errors() as $error) {
                echo "
                    <div class='alert alert-danger' role='alert'>
                        {$error}
                    </div>
                ";
            }
        }
    }
}

?>

<form action="" method="post">

    <div class="form-group row">
        <label for="username" class="col-sm-2 col-form-label">Username</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="username" id="username" value="<?=escape(Input::get("username"));?>" placeholder="TheKingOfTheNorth">
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>
    </div>

    <div class="form-group row">
        <label for="passwordConfirmation" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="passwordConfirmation" id="passwordConfirmation" placeholder="Confirm Password">
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" id="name" value="<?=escape(Input::get("name"));?>" placeholder="Jon Snow">
        </div>
    </div>

    <input type="hidden" name="token" value="<?=Token::generate();?>">

    <div class="form-group row">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Sign Up</button>
        </div>
    </div>

</form>